﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void BouttonJouer()
    {
        SceneManager.LoadScene("Pierre");
    }

    public void BouttonCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void BouttonQuitter()
    {
        Application.Quit();
    }
}
